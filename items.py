"""equipement and consumables"""

from core import Charac
from dice import *
from copy import copy

class Item(object):
    def __init__(self, name, description, price):
        self.name = name
        self.description = description
        self.price = price

    def describe(self):
        print(self.description)

    def __str__(self):
        return self.name

class Equipment(Item):
    def __init__(self, name, description, price, charac):
        Item.__init__(self, name, description, price)
        self.charac = charac

class Weapon(Equipment):
    def __init__(self, name, description, price, charac, attack_dice):
        Equipment.__init__(self, name, description, price, charac)
        self.attack_dice = attack_dice

class Armor(Equipment):
    def __init__(self, name, description, price, charac):
        Equipment.__init__(self, name, description, price, charac)

class Consumable(Item):
    def __init__(self, name, description, price, usage):
        Item.__init__(self, name, description, price)
        self.use = usage

def heal(character, amount):
    character.hp += amount


def enchant(item, modif=Charac(), new_dice=[]):
    new_item = copy(item)
    new_item.charac += modif
    if type(item) == Weapon:
        new_item.attack_dice.extend(new_dice)
    elif new_dice != []:
        raise ValueError("can not add dice to something that is not a weapon")

# consumables
potion = Consumable("potion", "une potion de soin", 1, lambda character: heal(character, 10))
super_potion = Consumable("super potion", "une potion de soin plus forte", 5, lambda character: heal(character, 50))
bomb = Consumable("bombe", "pour faire de gros dégats", 10, lambda character:  character.damage(sum(map(lambda x: x.cast(), [D10, D10, D10]))))

# armes

axe = Weapon("hache", "une hache solide, qui coupe aussi bien les ennemis que les arbres", 100, Charac(), [D8])
sword = Weapon("épée", "une épée basique, pour tuer du monstre", 100, Charac(), [D6])
stick = Weapon("baton", "je suis un baton", 1, Charac(strength=-1), [D4])

excalibur = enchant(sword, Charac(strength=10), [D6, D6])
fire_sword = enchant(sword, Charac(strength=3), [D6])

# armures
leather_armor = Armor("armure en cuir", "Une armure basique, d'une protection plus que utile", 150, Charac(1))
chainmail = Armor("Armure de maille", "Une cotte de maille, solide mais lourd", 250, Charac(2))

magic_armorleather = enchant(leather_armor, Charac(defense=3))

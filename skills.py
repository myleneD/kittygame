"""skills to make fight more intresting"""


class Skill(object):
    def __init__(self, name, max_charges, effect):
        self.name = name
        self.max_charges = max_charges
        self.effect = effect

class Skill_charge(object):
    def __init__(self, skill):
        self.skill = skill
        self.charges = skill.max_charges

    def __call__(self, caster, target):
        if self.charges > 0:
            self.skill.effect(caster, target)
            self.charges = self.charges - 1
        else:
            print ("Je ne peux pas utilise cette competence")

def damage_fireball(caster, other):
    damages = 8
    other.damage(damages)

def effect_double_strike(caster, other):
    caster.attack(other)
    caster.attack(other)


fire_ball = Skill("boule de feu", 10, damage_fireball)

double_strike = Skill("attaque double", 3, effect_double_strike)

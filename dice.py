"""emulate dice throwing"""

from random import randint, choice

# class
class Dice(object):
    def __init__(self, number):
        self.number = number

    # method / fonction = fonctionnement
    def cast(self):
        return randint(1, self.number)

class Special_dice(object):
    def __init__(self, faces):
        self.faces = faces

    def cast(self):
        return choice(self.faces)


D20 = Dice(20)
D6 = Dice(6)
D4 = Dice(4)
D8 = Dice(8)
D10 = Dice(10)
D12 = Dice(12)
D100 = Dice(100)

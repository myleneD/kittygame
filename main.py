"""main file for the kitty code game"""

from characters import *

def main():
    kitty = Hero()
    monster = bestiary["gobelin"](3)
    kitty.fight(monster)

main()

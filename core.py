"""core classes for kitty game"""

class Charac(object):
    def __init__(self, strength=0, defense=0, hp=0):
        self.strength = strength
        self.defense = defense
        self.hp = hp

    def __add__(self, other):
        return Charac(self.strength + other.strength,
                      self.defense + other.defense,
                      self.hp + other.hp)

def player_choice(options):
    if len(options) == 0:
        raise ValueError("choosing between 0 options")
    ask = None
    print("options disponibles :")
    for k in options:
        print(k)
    while ask not in options:
        ask = input("que faire ? :")
    callback = options[ask]
    return callback()

"""how to represent a map"""

class Matrix(object):
    def __init__(self, h, w):
        self.elements = [None for _ in range(h * w)]
        self.width = w
        self.height = h

    def __getitem__(self, index):
        (i, j) = index
        return self.elements[i * self.width + j]

    def __setitem__(self, index, value):
        (i, j) = index
        self.elements[i * self.width + j] = value


class Tile(object):
    def __init__(self, symbol, crossable):
        self.symbol = symbol
        self.crossable = crossable

    @classmethod
    def build(cls, symbol):
        tiles = {
            "#" : cls("#", False),
            "." : cls(".", True),
            "@" : cls("@", True)
        }
        return tiles[symbol]

class Map(object):
    def __init__(self, room):
        self.room = room

    @classmethod
    def read_file(cls, filename):
        map_file = open(filename, "r")
        read_line = map_file.readline()
        dimensions = [x for x in map(int, read_line.split())]
        h = dimensions[0]
        w = dimensions[1]
        room = Matrix(h, w)
        i = 0
        for line in map_file.readlines():
            j = 0
            for c in line:
                if c not in {'\n', '\r'}:
                    room[i, j] = Tile.build(c)
                j = j + 1
            i = i + 1
        map_file.close()
        return cls(room)

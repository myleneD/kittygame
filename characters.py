"""define characters for the kitty game"""

from dice import D4
from core import Charac, player_choice
from items import *
from skills import *

class Character(object):
    def __init__(self, name, charac, level):
        self.name = name
        self._level = level
        self.charac = charac

    @property
    def level(self):
        return self._level

    def total_charac(self):
        armor = self.armor.charac if self.armor else Charac()
        weapon = self.weapon.charac if self.weapon else Charac()
        return self.charac + armor + weapon

    @property
    def strength(self):
        return self.charac.strength

    @property
    def defense(self):
        return self.charac.defense

    def attack_dice(self):
        return []

    def damage(self, amount):
        self.charac.hp -= amount
        if self.charac.hp <= 0:
            self.charac.hp = 0

    def attack(self, other):
        force = 0
        for die in self.attack_dice():
            force = die.cast() + force

        wound = max(self.strength + force - other.defense, 0)
        print("{} attaques {} et lui fait {} point(s) de degats".format(self.name, other.name, wound))
        other.damage(wound)
        print("{} a encore {} point(s) de vie".format(other.name, other.charac.hp))

    def level_up(self):
        self._level += 1

    def combat_turn(self, other):
        self.attack(other)

    def fight(self, other):
        while self.charac.hp > 0 and other.charac.hp > 0:
            self.combat_turn(other)
            other.combat_turn(self)
        if self.charac.hp <= 0:
            print("{} est mort(e)".format(self.name))
        if other.charac.hp <= 0:
            print("{} est mort(e)".format(other.name))

class Hero(Character):
    def __init__(self):
        Character.__init__(self, "kitty", Charac(4, 4, 19), 1)
        self.items = []
        self.weapon = None
        self.armor = None
        self.gold = 0
        self.skills = [Skill_charge(double_strike)]

    def add_gold(self, gold):
        if (gold > 0):
            self.gold = self.gold + gold

    def loot(self, monster):
        gold, items = monster.loot()
        self.add_gold(gold)
        self.items.extend(items)

    def level_up(self):
        self._level += 1
        self.charac.strength = self.charac.strength + 1
        self.charac.defense = self.charac.defense + 1
        self.charac.hp = self.charac.hp + 2

    def use(self, item, target=None):
        target = target or self
        if type(item) == Consumable:
            item.use(self)
        elif type(item) == Weapon:
            if self.weapon:
                self.items.append(self.weapon)
            self.weapon = item
        elif type(item) == Armor:
            if self.armor:
                self.items.append(self.armor)
            self.armor = item
        else:
            print("vous ne pouvez pas utiliser {}".format(item.name))
            return
        self.items.remove(item)

    def combat_turn(self, other):
        skills = {charge.skill.name: lambda: charge(self, other) for charge in self.skills if charge.charges > 0}
        items = {item.name: lambda: self.use(item) for item in self.items}
        combat_choices = {
            "passer": lambda: None,
            "attaquer": lambda: self.attack(other),
        }
        if len(skills) > 0:
            combat_choices["compétence"] = lambda: player_choice(skills)
        if len(items) > 0:
            combat_choices["objet"] = lambda: player_choice(items)

        player_choice(combat_choices)

    @property
    def strength(self):
        return self.total_charac().strength

    @property
    def defense(self):
        return self.total_charac().defense

class Monster(Character):
    def __init__(self, name, attack, attack_dice, defense, hp, level, gold, drops):
        Character.__init__(self, name, Charac(hp=hp, defense=defense, strength=attack), level)
        self.lootable_gold = gold
        self.drops = drops
        self.dice = attack_dice

    def attack_dice(self):
        return self.dice

    def drop(self):
        return (self.lootable_gold, self.drops)

def make_gobelin(lvl):
    attack = lvl
    defense = lvl
    hp = 8 + 2 * lvl
    return Monster("gobelin", attack, [D4], defense, hp, lvl, 10, [stick])


def make_slime(lvl):
    attack = lvl + 2
    defense = lvl
    hp = 6 + 2 * lvl
    return Monster("slime", attack, [], defense, hp, lvl, 8, [])


bestiary = {
    "gobelin": make_gobelin,
    "slime": make_slime
}
